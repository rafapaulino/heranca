﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heranca
{
    class Program
    {
        static void Main(string[] args)
        {
            Pessoa pessoa = new Pessoa();
            pessoa.nome = "Rafael";
            pessoa.fone = "9977668454";

            PessoaFisica pessoaFisica = new PessoaFisica();
            pessoaFisica.nome = "Natália";
            pessoaFisica.endereco = "Avenida 1234";
            pessoaFisica.apelido = "Naty";
            pessoaFisica.cpf = "436536564543";
            pessoaFisica.idade = 25;
            pessoaFisica.fone = "847545657567676";

            PessoaJuridica pessoaJuridica = new PessoaJuridica();
            pessoaJuridica.nome = "Teste";
        }
    }
}
